package br.com.globalcode.lab01.dao;

import br.com.globalcode.lab01.model.Task;
import org.junit.Test;

public class TesteTaskDAO {
  
  @Test
  public void testCadastro() throws Exception {
  
    TaskDAO dao = new TaskDAO();
      
    Task taskA = null; 
    Task taskB = null; 
    
    Integer idtaskA = dao.salvar(taskA);
    Integer idtaskB = dao.salvar(taskB);
    
    Task x = dao.recuperarById(idtaskA);
    Task y = dao.recuperarById(idtaskB);
    
    System.out.println("x:" + x);
    System.out.println("y:" + y);
    
    System.out.println("A == X : " + (taskA == x) );
    System.out.println("B == Y : " + (taskB == y) );
    
    System.out.println("A equals X : " + taskA.equals(x));
    System.out.println("B equals Y : " + taskB.equals(y));
    
    x.setNome(x.getNome() + "");
    dao.atualizar(x);
    
    dao.removerById(y.getId());
    
  }

}
