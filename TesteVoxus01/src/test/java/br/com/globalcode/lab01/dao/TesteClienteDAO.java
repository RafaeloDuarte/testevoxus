package br.com.globalcode.lab01.dao;

import br.com.globalcode.lab01.model.Cliente;
import org.junit.Test;

public class TesteClienteDAO {
  
  @Test
  public void testCadastro() throws Exception {
  
    ClienteDAO dao = new ClienteDAO();
      
    Cliente cliA = null; //new Cliente( ???, ???, ... );
    Cliente cliB = null; //new Cliente( ???, ???, ... );
    
    Integer idCliA = dao.salvar(cliA);
    Integer idCliB = dao.salvar(cliB);
    
    Cliente x = dao.recuperarById(idCliA);
    Cliente y = dao.recuperarById(idCliB);
    
    System.out.println("x:" + x);
    System.out.println("y:" + y);
    
    System.out.println("A == X : " + (cliA == x) );
    System.out.println("B == Y : " + (cliB == y) );
    
    System.out.println("A equals X : " + cliA.equals(x));
    System.out.println("B equals Y : " + cliB.equals(y));
    
    x.setNome(x.getNome() + " Pereira de Oliveira");
    dao.atualizar(x);
    
    dao.removerById(y.getId());
    
  }

}
