package br.com.globalcode.lab01.dao;

import br.com.globalcode.lab01.model.Task;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class TaskDAO {

    EntityManagerFactory factory;
    
    public TaskDAO() {
        factory = Persistence.createEntityManagerFactory("eCommerce");
    }

    public Task recuperarById(Integer id) {
        return null;
    }

    public Integer salvar(Task c) {
        return null;
    }

    public void atualizar(Task c) {
    }

    public Task removerById(Integer id) {
        return null;
    }
}
