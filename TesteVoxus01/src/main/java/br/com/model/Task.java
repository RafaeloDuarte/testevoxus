package br.com.globalcode.lab01.model;

import java.io.Serializable;
import javax.persistence.*;


@Entity
@Table(name="tb_task")
public class Task implements Serializable {
    
    @Id @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="id_task")
    private Integer id;
    @Column(name="id_task")
    private String nome;
    @Column(name="id_task")
    private String descricao;
    @Column(name="id_task")
    private Integer qtdeAnexos;
    @Column(name="id_task")
    private String prioridade;
    @Column(name="id_task")
    private String usuario;

    
    public Task(){
    }
    
    public Task(String nome, int qtdeAnexos, String prioridade, String descricao,
            String usuario) {
        this.nome = nome;
        this.descricao = descricao;
        this.qtdeAnexos = qtdeAnexos;
        this.prioridade = prioridade;
        this.usuario = usuario;

    }
    
    public Task(Integer id, String nome, int qtdeAnexos, String prioridade, String descricao,
            String usuario) {
        this(nome,qtdeAnexos,prioridade,descricao,usuario);
        this.id = id;
    }
    
    public String getNome(){
        return nome;
    }
    
    public void setNome(String nome){
        this.nome = nome;
    }
    
    public Integer getId(){
        return id;
    }
    
    public void setId(Integer id){
        this.id = id;
    }
    
    public String getdescricao(){
        return descricao;
    }
    
    public void setdescricao(String descricao){
        this.descricao = descricao;
    }
    
    public int getqtdeAnexos(){
        return qtdeAnexos;
    }
    
    public void setqtdeAnexos(int qtdeAnexos){
        this.qtdeAnexos = qtdeAnexos;
    }
    
    public String getprioridade(){
        return prioridade;
    }
    
    public void setprioridade(String prioridade){
        this.prioridade = prioridade;
    }
    
    public String getusuario() {
        return usuario;
    }
    
    public void setusuario(String usuario) {
        this.usuario = usuario;
    }
    
    @Override
    public String toString() {
        return getNome() + " - " + getId();
    }

    
}